1. Як можна оголосити змінну у Javascript?

Це var, let та const.


2. Що таке рядок (string) і як його створити (перерахуйте всі можливі варіанти)?

Літеральний спосіб: let myString = "Hello, World!";
Використання функцій створення рядків: let myString = String(42);
Конкатенація рядків: let myString = "Hello, " + "World!";
Використання форматування рядків: let name = "Alice";
                                  let age = 25;
                                  let myString = `My name is ${name} and I'm ${age} years old.`;


3. Як перевірити тип даних змінної в JavaScript?

Оператор typeof: 
let variable = "Hello";
console.log(typeof variable); // "string"

Метод instanceof:
let array = [1, 2, 3];
console.log(array instanceof Array); // true

Порівняння зі спеціальними значеннями:
let value = null;
if (value === null) {
  console.log("The variable is null.");
}


4. Поясніть чому '1' + 1 = 11.

'1' є рядком, а 1 є числом. При виконанні операції '1' + 1, JavaScript спочатку спробує 
перетворити число 1 в рядок і потім з'єднати його з рядком '1', що призводить до 
отримання рядка '11'.